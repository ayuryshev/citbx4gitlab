. $CITBX_TOOL_DIR/env-setup/common.sh

INSTALL_PKGS=()

if setup_component_enabled base-pkgs; then
    _sudo dnf -y install dnf-plugins-core
    _sudo dnf config-manager --add-repo \
        https://download.docker.com/linux/fedora/docker-ce.repo
    INSTALL_PKGS+=(docker-ce gawk python3-PyYAML jq ca-certificates iproute)
fi

if setup_component_enabled git-lfs; then
    INSTALL_PKGS+=(git-lfs)
fi

if [ "${#INSTALL_PKGS[@]}" -gt 0 ]; then
    print_info "Installing packages..."
    _sudo dnf install -y "${INSTALL_PKGS[@]}"
fi

if setup_component_enabled base-pkgs; then
    install_pkgs_postinst
fi

if setup_component_enabled ca-certs; then
    print_info "Installing CA certificates..."
    # Add custom SSL ROOT CAs
    install_ca_certificates_system
    install_ca_certificates_docker
fi

if setup_component_enabled docker-cfg; then
    print_info "Configuring docker..."

    write_daemon_json

    _sudo ip link del docker0 2>/dev/null || true
    if systemctl status dbus.socket /dev/null 2>&1; then
        _sudo service docker restart
    fi
fi

if setup_component_enabled ci-toolbox; then
    print_info "Installing the CI toolbox..."
    fetch_ci_toolbox_pkg rpm
    _sudo dnf install -y "$CITBX_TMPDIR/ci-toolbox.rpm"
    print_info "CI Toolbox setup complete"
fi
