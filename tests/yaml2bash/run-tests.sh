#!/usr/bin/env bash
# citbx4gitlab: CI toolbox for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2018-2020 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

TEST_OK_NB=0
TEST_KO_NB=0
: ${CI_PROJECT_DIR:="$(git rev-parse --show-toplevel 2>/dev/null || true)"}

# Print an error message and exit with error status 1
test_print_critical() {
    >&2 printf "\033[91m[CRIT] %s\033[0m\n" "$@"
    exit 1
}

# Print a note message
test_print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Pring an info message
print_info() {
    printf "\033[92m[INFO] %s\033[0m\n" "$@"
}

# Pring an info message
test_msg_ok() {
    printf "\033[92m[<OK>] %s\033[0m\n" "$@"
    ((++TEST_OK_NB))
}

# Print an error message
test_msg_ko() {
    >&2 printf "\033[91m[<!!>] %s\033[0m\n" "$@"
    ((++TEST_KO_NB))
}

test_finish() {
    local retcode=$?
    rm -f output.log
    if [ $retcode -ne 0 ]; then
        test_print_critical "Exit on fatal error"
    fi
    test_print_note  "* Test number : $((TEST_OK_NB + TEST_KO_NB))" \
                "* Successes   : $TEST_OK_NB" \
                "* Failures    : $TEST_KO_NB"

    if [ $TEST_KO_NB -eq 0 ]; then
        print_info "Test suite execution succeeded"
    else
        test_print_critical "Test suite execution failed"
    fi
}

trap test_finish EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM

run_check() {
    local arglist arg command \
            match_list not_match_list \
            line
    if ! arglist=$(getopt -o "m:M:" -n "$0 " -- "$@"); then
        test_print_critical "Usage run_check" \
            "        -m <val> must match with..."
            "        -M <val> must not match with..."
    fi
    eval set -- "$arglist";
    # Store the global bashopts properties
    while true; do
        arg=$1
        shift
        case "$arg" in
            -m) match_list+=("$1");      shift;;
            -M) not_match_list+=("$1");  shift;;
            --) break;;
            *)  test_print_critical "Fatal error";;
        esac
    done

    # Read output
    local output="$(eval "
        $JOB_DATA
        $(printf "%s\n" "$@")
    ")"
    local output_lines=()
    while read -r line; do
        output_lines+=("$line")
    done <<< "$output"

    # Check matches
    if [ ${#match_list[@]} -gt 0 ]; then
        set -- "${match_list[@]}";
        for line in "${output_lines[@]}"; do
            if [ $# -eq 0 ]; then
                break
            elif [[ "$line" =~ $1 ]]; then
                test_msg_ok "'$1' match as expected - line: $line"
                shift
            fi
        done
        if [ $# -ne 0 ]; then
            test_msg_ko "'$1' don't match as expected or not in the right order$(
                echo
                echo "Output:"
                printf "\t> %s\n" "${output_lines[@]}"
            )"
            shift
            while [ $# -ne 0 ]; do
                test_msg_ko "'$1' match not tested due to previous error"
                shift
            done
        fi
    fi

    for m in "${not_match_list[@]}"; do
        if grep -E "$m"  <<< "$output"; then
            test_msg_ko "'$m' match and shouldn't"
        else
            test_msg_ok "'$m' don't match as expected"
        fi
    done
}

run_gitlabci_yml2bash() {
    local test=$1
    shift || test_print_critical "Usage: run_gitlabci_yml2bash <test_name> [env...]"
    JOB_DATA='
        print_critical() {
            GITLABCI_YML2BASH_CRITICALS+=("$@")
        }
        print_warning() {
            GITLABCI_YML2BASH_WARNINGS+=("$@")
        }
        '"$(
        test ! -f "$CI_PROJECT_DIR/tests/yaml2bash/$test/env-test-case.sh" \
            || source "$CI_PROJECT_DIR/tests/yaml2bash/$test/env-test-case.sh"
        env "${TEST_CASE_ENV[@]}" "CI_PROJECT_DIR=$CI_PROJECT_DIR" CI_CONFIG_PATH=tests/yaml2bash/$test/.gitlab-ci.yml \
        "$@" python3 "$CI_PROJECT_DIR/ci-toolbox/gitlabci_yml2bash.py")"
    if [ "$TEST_DEBUG" == "true" ]; then
        echo "$JOB_DATA"
    fi
    local output="$(eval '
        '"$JOB_DATA"'
        test ${#GITLABCI_YML2BASH_CRITICALS[@]} -eq 0 || printf "%s\n" "${GITLABCI_YML2BASH_CRITICALS[@]}"
    ')"
    local critical_messages
    test -z "$output" || readarray -t critical_messages <<< "$output"
    if [ ${#critical_messages[@]} -eq 0 ]; then
        test_msg_ok "Process FILE [tests/yaml2bash/$test/.gitlab-ci.yml] > JOB[$CI_JOB_NAME]"
    else
        test_msg_ko "${critical_messages[@]}"
    fi
    test ! -f "$CI_PROJECT_DIR/tests/yaml2bash/$test/after-test-case.sh" \
        || source "$CI_PROJECT_DIR/tests/yaml2bash/$test/after-test-case.sh"
}

if [ "$1" == "-v" ]; then
    TEST_DEBUG=true
    shift
fi

if [ $# -eq 0 ]; then
    set -- $(find "$CI_PROJECT_DIR/tests/yaml2bash" -mindepth 1 -maxdepth 1 -type d -printf "%f ")
fi

for test in "$@"; do
    unset CI_JOB_NAME
    test_print_note "[tests/yaml2bash] Start test: [$test] > ROOT"
    run_gitlabci_yml2bash $test
    readarray -t jobs <<< "$(
        eval "$JOB_DATA"
        printf "%s\n" "${CITBX_JOB_LIST[@]}"
    )"
    for CI_JOB_NAME in "${jobs[@]}"; do
        test_print_note "[tests/yaml2bash] Start test [$test] > Job[$CI_JOB_NAME]"
        run_gitlabci_yml2bash $test CI_JOB_NAME="$CI_JOB_NAME"
    done
    print_info "[tests/yaml2bash] test [$test] OK"
done
