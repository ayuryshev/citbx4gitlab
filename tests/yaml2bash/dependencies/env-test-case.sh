
case "$CI_JOB_NAME" in
    build1)
        TEST_CASE_ENV=('CITBX_JOB_DEPENDENCY_STRATEGY=needs:artifacts')
    ;;
    build2)
        TEST_CASE_ENV=('CITBX_JOB_DEPENDENCY_STRATEGY=needs')
    ;;
    build3)
        TEST_CASE_ENV=('CITBX_JOB_DEPENDENCY_STRATEGY=default')
    ;;
    test1)
        TEST_CASE_ENV=('CITBX_JOB_DEPENDENCY_STRATEGY=needs:artifacts')
    ;;
    test2)
        TEST_CASE_ENV=('CITBX_JOB_DEPENDENCY_STRATEGY=default')
    ;;
    test3)
        TEST_CASE_ENV=('CITBX_JOB_DEPENDENCY_STRATEGY=default')
    ;;
    deploy*)
        TEST_CASE_ENV=('CITBX_JOB_DEPENDENCY_STRATEGY=default')
    ;;
    '')
        
    ;;
esac
