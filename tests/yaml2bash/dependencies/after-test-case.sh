
case "$CI_JOB_NAME" in
    build1)
        run_check \
            -m "^$" -M '^.+$' \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    build2)
        run_check \
            -m "^$" -M '^.+$' \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    build3)
        run_check \
            -m "^$" -M '^.+$' \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    test1)
        run_check \
            -m "^build1$" \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    test2)
        run_check \
            -m "^build1 build2$" \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    test3)
        run_check \
            -m "^build1 build2 build3$" \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    deploy1)
        run_check \
            -m "^build1 build2 build3 test1 test2 test3$" \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    deploy2)
        run_check \
            -m "^build1 build2 test1$" \
            -- 'echo ${CITBX_JOB_DEPENDENCIES_LIST[*]}'
    ;;
    '')
        run_check \
            -m "^build1 build2 build3 test1 test2 test3 deploy1 deploy2$" \
            -- 'echo ${CITBX_JOB_LIST[*]}'
    ;;
esac
